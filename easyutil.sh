#!/bin/bash

# Move existing articles to separate directories and create an index file
for file in content/posts/*.md; do
  dir="content/posts/$(basename "$file" .md)"
  mkdir -p "$dir"
  mv "$file" "$dir"/index.md
done


